from behave import fixture
from selenium import webdriver
import os

SELENIUM = os.getenv('SELENIUM', 'http://localhost:4444/wd/hub')


def before_all(context):
    context.browser = webdriver.Remote(
        desired_capabilities=webdriver.DesiredCapabilities.FIREFOX,
        command_executor=SELENIUM,
    )


def after_all(context):
    context.browser.quit()
