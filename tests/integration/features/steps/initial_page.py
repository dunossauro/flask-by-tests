from behave import given, then


@given(u'que esteja na página inicial')
def step_impl(context):
    ...


@then(u'"{text}" deve estar na página')
def step_impl(context, text):
    ...
