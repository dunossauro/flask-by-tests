from unittest import TestCase
from flask import url_for
from app import create_app


class TestApp(TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.testing = True
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def test_index_route_should_return_flask_by_tests(self):
        request = self.client.get(url_for('index'))
        self.assertEqual(request.data.decode(), 'flask-by-tests')
