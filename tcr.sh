#! /bin/bash

# Usage:
# ./tcr.sh -> test && commit || revert
# ./tcr.sh squash "my message" -> get all comits with "TCR" message squash and commit with your message

python_unittest(){
  python -m unittest discover tests -v 2>&1 | grep 'OK'
}

git_add_commit(){
  git add .
  git commit -am "TCR"
}

tcr(){
   python_unittest && git_add_commit || git clean -fd
}

squash(){
  if [[ -z "$1" ]]; then
    echo -e "Please insert commit message"
    return
  fi
  COMMIT_HASH=$(git log --format=oneline | grep -v TCR | cut -d " " -f 1 | head -n 1)
  git reset --soft $COMMIT_HASH
  git commit -m $1
}

case $1 in
  squash) squash $2;;
  *) tcr;;
esac
